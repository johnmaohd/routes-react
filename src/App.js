import {BrowserRouter, Route, Routes} from "react-router-dom";
import HomePage from "./Pages/HomePage";
import AboutPage from "./Pages/AboutPage";
import Users from "./Pages/Users";
import Products from "./Pages/Products";
import NotFoundPage from "./Pages/NotFoundPage";
import Navbar from "./components/Navbar";

export default function App() {
    return (
        <BrowserRouter>
            <Navbar/>
            <Routes>
                <Route path="/" element={<HomePage/>}/>
                <Route path="/about" element={<AboutPage/>}/>
                <Route path="/users" element={<Users/>}/>
                <Route path="/products" element={<Products/>}/>
                <Route path="*" element={<NotFoundPage/>}/>
            </Routes>

        </BrowserRouter>
    );
}
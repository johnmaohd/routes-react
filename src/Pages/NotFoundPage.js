import styles from "./NotFoundPage.module.css"
export default function NotFoundPage() {
    return <div className={styles.notfound}>404 not found page</div>;
}